<?php

/**
 * Parse a given KML string.
 * 
 * @todo: Add KMZ support (grab code from parser_csv).
 *
 * @param $url 
 *   URL that points to a valid KML file.
 * @return 
 *   a feed object with parsed and normalized data
 */
function _parser_kml_parse($url) {
  // Create FeedAPI feed object.
  $feed = new stdClass();
  $feed->title = t('Placemarks from !url', array('!url' => $url));
  $feed->description = '';
  $feed->options->link = $url;
  $feed->items = array();

  // Download and parse document @ URL
  $kml_string = _parser_common_syndication_feedapi_get($url);  
  $reader = new XMLReader();
  $reader->xml($kml_string);
  $handler = new parserKMLPlacemarkHandler();
  parser_kml_get_elements($reader, $handler, "Placemark", $feed->items);
  $reader->close();
  return $feed;
}

/**
 * @param $reader
 *   An instance of XMLReader.
 * @param $handler
 *   An implementation of the parserKMLHandler interface.
 * @param $element_name
 *   The name of the element to parse.
 * @param $items
 *   The array to add parsed elements to.
 * @param $limit;
 *   The max number of elements to parse.
 */
function parser_kml_get_elements($reader, $handler, $element_name, &$items, $limit = false) {
  $field = null;
  while($reader->read()) {
    if ($reader->nodeType != XMLReader::END_ELEMENT) {
      if ($reader->localName == $element_name) {
        $handler->reset($reader);
      }
      elseif (!$reader->isEmptyElement) {
        $field = &$handler->element_process($reader, $field);
      }
    }
    elseif ($reader->localName == $element_name && isset($handler)) {
      $item = $handler->element_end();
 
      // If this is a place mark, attach namespaces used in placemark and
      // reset namespaces.
      if ($element_name == 'Placemark') {
        $item->namespaces = parserKMLNamespaces::getNameSpaces();
        parserKMLNamespaces::resetNameSpaces();
      }
      $items[] = $item;
      if (is_numeric($limit) && count($items) >= $limit)  {
        return;
      }
    }
  }
}

interface parserKMLHandler  {
  /**
   * @param $reader
   *   An instance of XMLReader
   */
  public function reset($reader);
  
  /**
   * @param $reader
   *   An instance of XMLReader
   * @param $field
   *   A reference to a item on the internal feed item
   * @return
   *   A reference to a item on the internal feed item
   */
  public function &element_process($reader, &$field);
  
  /**
   * @return
   *   A parsed and processed element to at the the items array.
   */
  public function element_end();
}

class parserKMLPlacemarkHandler implements parserKMLHandler {
  private $item;
  public $depth;

  function reset($reader) {
    $this->depth = $reader->depth;

    $this->item = new stdClass();
    $this->item->title = t('No title');
    $this->item->description = '';
    $this->item->options = new stdClass();
    $this->item->options->kml_type = 'placemark';

    if ($reader->moveToAttribute('id')) {
      $item->options->placemark = $reader->value;
    }
    return;
  }

  /**
   * @todo: 
   *   * comment
   *   * consolidate null pointer referencing and usage of readInnerXml() for simple 
   *     plain text retrievals.
   * 
   * @param $reader XMLreader object
   * @param $field, a reference to the field on the current item that we're populating.
   * @return reference to current field.
   */
  function &element_process($reader, &$field) {
    switch ($reader->localName) {
      case 'name':
        return $this->item->title;
      case 'description':
        return $this->item->description;
      case 'link':
        $this->item->options->original_url = '';
        return $this->item->options->original_url;
      case 'Metadata':
        $this->item->options->metadata = '';
        $handler = new parserKMLMetadataHandler();
        $handler->reset($reader);
        $data = array();
        parser_kml_get_elements($reader, $handler, 'Metadata', $data, 1);
        $this->item->options->metadata = array_pop($data);
        return null;
      case 'coordinates':
        // Normalize to FeedAPI convention.
        list($lat, $lon) = explode(',', $reader->readInnerXml());
        $this->item->options->location->latitude[0] = $lat;
        $this->item->options->location->longitude[0] = $lon;
        return null;
      default:
        if ($field !== null) {
          if ($reader->nodeType === XMLReader::TEXT || $reader->nodeType === XMLReader::CDATA) {
            $field = $reader->value;
          }
          unset($field);
        }
        break;
    }
    return null;
  }

  function element_end() {
    // Generate GUID from URL + hash of serialized placemark. Let's not use the position of this
    // placemark in feed, uniqueness of item doesn't change w/ position in KML feed.
    $this->item->options->guid = hash('md5', serialize($this->item));
    return $this->item;
  }
}

class parserKMLMetadataHandler implements parserKMLHandler {
  private $item;
  private $elem;
  public $depth;

  function reset($reader) {
    $this->depth = $reader->depth;
    $item = array();
  }

  function &element_process($reader, &$field) {
    switch ($reader->nodeType) {
      case XMLReader::ELEMENT:
        $this->elem = $reader->name;
        break;
      case XMLReader::TEXT:
        if ($this->elem) {
          parserKMLNamespaces::registerNamespace($reader, $this->elem);
          $this->item[$this->elem] = $reader->value;
          $this->elem = false;
        }
        break;
    }
    return null;
  }

  function element_end() {
    return $this->item;
  }
}

/**
 * Static class used to register namespaces.
 */
class parserKMLNamespaces {
  static private $nameSpaces = array();
  
  /**
   * Register namespace in given $elem.
   * 
   * @todo: Find out whether there is not a better way of retrieving
   *        all used name spaces in a KML feed or an item.
   *        Make parserKMLHandler anonymous class 
   *        and move this function up to parserKMLHandler
   *
   * @param XMLReader $reader
   * @param string $elem
   */
  static function registerNamespace($reader, $elem) {
    // Resolve namespaces.
    $e = explode(':', $elem);
    if (count($e) > 1) {
      if ($ns = $reader->lookupNamespace($e[0])) {
        parserKMLNamespaces::$nameSpaces[$e[0]] = $ns;
      }
    }
  }

  /**
   * Returns all nameSpaces registered during parsing.
   * 
   * @return Array where keys are namespace handles and values are
   *   namespace URI's
   */
  static function getNamespaces() {
    return parserKMLNamespaces::$nameSpaces;
  }
  
  /**
   * Resets all namespaces.
   */
  static function resetNamespaces() {
    parserKMLNamespaces::$nameSpaces = array();
  }
}