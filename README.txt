KML Parser for FeedAPI
======================

This module downloads, parses and normalizes KML[1] feeds for the FeedAPI framework. 

Usage: install with FeedAPI and configure as active parser on your feed content type.

[1] http://en.wikipedia.org/wiki/Keyhole_Markup_Language